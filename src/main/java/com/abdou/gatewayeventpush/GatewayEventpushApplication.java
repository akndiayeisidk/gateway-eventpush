package com.abdou.gatewayeventpush;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class GatewayEventpushApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayEventpushApplication.class, args);
    }

}
